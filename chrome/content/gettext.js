/*
 * Bloom-Web-Browser: The web browser for Bloom
 * Copyright (c) 2009, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 2.1, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 */

var gettext;
var ngettext;

(function() {
  try 
  {
    // Using an anonymous function to create a static gettext service variable
    var pagesService = (Components.classes["@bloom.org/mwb-pages-service;1"]
			.getService(Components.interfaces.MwbIPagesService));
    gettext = function(str) { return pagesService.gettext(str); }
    ngettext = function(singular, plural, count)
      {
	return pagesService.ngettext(singular, plural, count);
      }
  }
  catch (e)
  {
    gettext = function(str) { return str; }
    ngettext = function(singular, plural, count)
      {
	return singular;
      }
  }
})();

// Make a shorter alias that will also be used to mark strings for
// translation.
var _ = gettext;
