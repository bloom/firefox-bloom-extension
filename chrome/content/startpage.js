/*
 * Bloom-Web-Browser: The web browser for Bloom
 * Copyright (c) 2009, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 2.1, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 */

var dragged = "";

function binaryToHex(aRaw)
{
  return [('0' + aRaw.charCodeAt(i).toString(16)).slice(-2)
          for (i in aRaw)].join('');
}

// Caculate MD5 checksum for a string
function caculateMD5CheckSum(aString)
{
  if (!aString)
      aString = "";

  var hasher = Components.classes["@mozilla.org/security/hash;1"]
                         .createInstance(Components.interfaces.nsICryptoHash);
  hasher.init(Components.interfaces.nsICryptoHash.MD5);

  var stream = Components.classes['@mozilla.org/io/string-input-stream;1']
                         .createInstance(Components.interfaces.nsIStringInputStream);
  stream.setData(aString, aString.length);
  hasher.updateFromStream(stream, stream.available());

  return binaryToHex(hasher.finish(false));
}

// Find the thumbPath of the given url
function getThumbPath (uri)
{
  var prefs = Components.classes["@mozilla.org/preferences-service;1"]
    .getService(Components.interfaces.nsIPrefBranch);

  //Comments while no thumbnail created
  //if (prefs.getBoolPref("moblin.thumbnails.enabled")) {
    var dirService = Components.classes["@mozilla.org/file/directory_service;1"].
      getService(Components.interfaces.nsIProperties);
    var homeDir = dirService.get("Home", Components.interfaces.nsIFile).path;

    var thumbSubDir = ".thumbnails/large";
    //thumbSubDir = prefs.getComplexValue("moblin.thumbnails.path",
    //                       Components.interfaces.nsISupportsString).data;

    return homeDir + "/" + thumbSubDir + "/" + caculateMD5CheckSum(uri) + ".png";
  //}
}

// Construct the "favorites" div (drop indicate if the build is called by a drag and drop event)
function buildPinnedPages(drop)
{
  var iconsvc = Components.classes["@mozilla.org/browser/favicon-service;1"].
                  getService(Components.interfaces.nsIFaviconService);

  var defaultMsgDiv = document.getElementById('pinned_pages_default_msg');
  var ManagePinnedPagesMsgDiv = document.getElementById('pinned_pages_manage_msg');
  var pinnedPagesDiv = document.getElementById("favorites");

  // Get the "old" style to push later on the "new" bookmarks
  var dropStyle = new Array();
  if (!drop) {
     var divs = document.getElementsByTagName('div');
     for(var i=0; i<divs.length; i++) {
        if(divs[i].className == "pinned_page_thumbnail thumbnail") {
           var dropId = divs[i].getAttribute("id");
           dropStyle[dropId] = divs[i].getAttribute("style");
        }
     }
  }

  // Remove all existing pinned pages
  pinnedPagesDiv.innerHTML = "";

  // Get all the bookmarks info in the database
  var historyService = Components.classes["@mozilla.org/browser/nav-history-service;1"]
    .getService(Components.interfaces.nsINavHistoryService);
  var db = historyService.QueryInterface(Components.interfaces.nsPIPlacesDatabase).DBConnection;
  var q = "SELECT DISTINCT b.title as title, p.url as uri, f.url as favicon, b.id as id " +
          "FROM moz_bookmarks as b INNER JOIN moz_places as p ON b.fk = p.id " +
                                  "LEFT JOIN moz_favicons as f ON p.favicon_id = f.id " +
          "WHERE b.type = 1 AND p.hidden <> 1 AND " +
                "b.parent <> (SELECT ifnull((SELECT id FROM moz_bookmarks WHERE title = 'Mozilla Firefox' LIMIT 1), -1)) AND " +
                "b.title <> 'Get Bookmark Add-ons' AND " +
                "b.title <> 'Getting Started' AND " +
                "SUBSTR(p.url, 1, 7) <> 'bloom:' " +
          "ORDER BY b.id";

  var stmt;
  stmt = db.createStatement(q);
  // Create a bookmark per result
  while (stmt.executeStep()) {
    var page = {uri: "", title: "", favicon: ""};
    page.title = stmt.getString(0);
    page.uri = stmt.getString(1);
    page.favicon = stmt.getString(2);
    page.id = stmt.getString(3);

    // Create the main bokkmark div
    var div = document.createElement("div");
    div.setAttribute("class", "pinned_page_div");
    div.setAttribute("draggable", "true");
    div.setAttribute("ondragstart", "return dragStart(event)");
    div.setAttribute("ondragend", "return dragEnd(event)");
    div.setAttribute("ondragenter", "return dragEnter(event)");
    div.setAttribute("ondrop", "return dragDrop(event)");
    div.setAttribute("ondragover", "return dragOver(event)");
    div.setAttribute("ondragleave", "return dragLeave(event)");
    div.setAttribute("onmouseout", "return outDiv(event,3)");
    div.setAttribute("id", "div"+page.id);

    var a = document.createElement("a");
    a.setAttribute("class", "pinned_page_div_link");
    a.setAttribute("id", "drag"+page.id);

    // Create the bookmark's thumbnail and give it its old style if there is one
    var thumbnailDiv = document.createElement("div");
    thumbnailDiv.setAttribute("class", "pinned_page_thumbnail thumbnail");
    thumbnailDiv.setAttribute("id", "drop"+page.id);
    thumbnailDiv.setAttribute("onmouseover", "return overDiv(event,4)");
    if (dropStyle["drop"+page.id] != null) thumbnailDiv.setAttribute("style", dropStyle["drop"+page.id])
    else thumbnailDiv.setAttribute("style", "background: url(file://" + getThumbPath (page.uri)+") no-repeat center top; margin-top: 5px;");

    a.appendChild(thumbnailDiv);

    // Create elements for the close button. The outer div is
    // just used to make the position for the inner element
    // relative to the top of the thumbnail
    var closeOuter = document.createElement('div');
    closeOuter.setAttribute('class', 'close-button-outer');
    var closeInner = document.createElement('a');
    closeInner.setAttribute('class', 'close-button-inner waiting');
    closeInner.setAttribute('id', 'button'+page.id);
    closeInner.setAttribute('href', 'javascript:');
    closeOuter.appendChild(closeInner);

    // Create a closure with the url and use it as a callback
    // to unpin the page when the link is clicked
    let (id = page.id) {
      closeInner.addEventListener("click",
                                  function(e) {
                                    deleteBookmark (id,e);
                                    e.stopPropagation();
                                  },
                                  false);
    }

    // Create the icon and the texte below the thumbnail
    var img = document.createElement("img");
    img.setAttribute("class", "pinned_page_favicon");
    img.setAttribute("name", page.title);
    if (page.favicon)
      img.setAttribute("src", page.favicon);
      img.setAttribute("height", "16px");
      img.setAttribute("width", "16px");
    a.appendChild(img);

    var span = document.createElement("span");
    span.setAttribute("class", "pinned_page_title");
    span.appendChild(document.createTextNode(page.title.slice(0,18)));
    if (page.title.length > 18) span.appendChild(document.createTextNode("..."));

    a.appendChild(span);

    a.appendChild(document.createElement("br"));
    a.setAttribute("href", page.uri);

    div.appendChild(closeOuter);
    div.appendChild(a);

    pinnedPagesDiv.appendChild(div);

  }

  // Show the information div if there is no bookmark
  if (pinnedPagesDiv.childNodes.length != 0)
    defaultMsgDiv.setAttribute("class", "waiting");
  else
    defaultMsgDiv.setAttribute('class', 'inside');

  stmt.reset();
  stmt.finalize();
  stmt = null;

  // Test the connection
  ConnectToNet('http://www.google.com');

  // This will update the "favorites'" div after 5s+1s*nb of bookmarks
  setTimeout("buildPinnedPages(false)",5000 + 1000*pinnedPagesDiv.childNodes.length);
}

// Functions to manage drag and drop
function dragStart(ev) {
    var id = ev.target.getAttribute('id');
    if (id.slice(0,4) != "drag") return false;
    dragged = id;
    return true;
}

function dragEnter(ev) {
    var id = ev.target.getAttribute('id');
    if (id == null) return false;
    else if (id.slice(0,4) == "drop") cut=4;
    else if (id.slice(0,3) == "div") return;
    else return false;
    var a = document.getElementById("drop"+id.slice(cut));
    if (a == null) return false;
    var style = a.getAttribute("style");
    var i = 0;
    move();
    function move() {
      if (dragged.slice(4) == id.slice(cut)) {
        style += " border: 3px solid #d5dcdf; margin-top: "+(5-i)+"px;";
        a.setAttribute("style", style);
        i++;
        if (i >= 3) return;
      }
      else {
        style += " border: 3px solid #55ccff; margin-top: "+(5-i)+"px;";
        a.setAttribute("style", style);
        i++;
        if (i >= 5) return;
      }
      setTimeout(move,30);
    }
    return false;
}

function dragOver(ev) {
    var id = ev.target.getAttribute('id');
    if (id == null) return false;
    if (dragged.slice(4) == id.slice(4)) return true;
    else return false;
}

function dragLeave(ev) {
    var id = ev.target.getAttribute('id');
    if (id == null) return false;
    else if (id.slice(0,4) == "drop") cut=4;
    else if (id.slice(0,3) == "div") return;
    else return false;
    var a = document.getElementById("drop"+id.slice(cut));
    if (a == null) return false;
    var style = a.getAttribute("style");
    if (dragged.slice(4) == id.slice(cut)) var i = 3;
    else var i = 0;
    move();
    function move() {
      style += " border: 3px solid #d5dcdf; margin-top: "+i+"px;";
      a.setAttribute("style", style);
      if (i >= 5) return;
      i++;
      setTimeout(move,50);
    }
    a.setAttribute("onmouseover", "border-color:#c5a3fc");
    return true;
}

function dragEnd(ev) {
    buildPinnedPages(true);
    return true;
}

function dragDrop(ev) {
    var id = ev.target.getAttribute('id');
    if (id == null) return false;
    else if (id.slice(0,4) == "drop") cut=4;
    else if (id.slice(0,3) == "div") return false;
    else return false;
    ev.stopPropagation();
    var historyService = Components.classes["@mozilla.org/browser/nav-history-service;1"]
    .getService(Components.interfaces.nsINavHistoryService);
    var db = historyService.QueryInterface(Components.interfaces.nsPIPlacesDatabase).DBConnection;
    db.createStatement("UPDATE moz_bookmarks SET id ="+36000+" WHERE id="+dragged.slice(4)+"; ").execute();
    db.createStatement("UPDATE moz_bookmarks SET id ="+dragged.slice(4)+" WHERE id="+id.slice(cut)+"; ").execute();
    db.createStatement("UPDATE moz_bookmarks SET id ="+id.slice(cut)+" WHERE id="+36000+"; ").execute();
    dragged = "";
    buildPinnedPages(true);
    return false; // return false so the event will not be propagated to the browser
}

// Functions to manage mouse "in" and "out" div's events
function overDiv(ev,cut) {
    function move() {
      style += " border: 3px solid #c5a3fc; margin-top: "+i+"px;";
      a.setAttribute("style", style);
      if (i <= 0) return;
      i--;
      setTimeout(move,30);
    }
    var id = ev.target.getAttribute('id');
    if (id == null) return false;
    var button = document.getElementById("button"+id.slice(cut));
    if (button == null) return false;
    button.setAttribute("class", "close-button-inner inside");
    var a = document.getElementById("drop"+id.slice(cut));
    if (a == null) return true;
    var style = a.getAttribute("style");
    if (style.slice(-4,-3) != "0") {
        var i = 5;
        move();
    }
    return true;
}

function outDiv(ev,cut) {
    function move() {
      style += " border: 3px solid #d5dcdf; margin-top: "+i+"px;";
      a.setAttribute("style", style);
      if (i >= 5) return;
      i++;
      setTimeout(move,30);
    }
    var id = ev.target.getAttribute('id');
    if (id == null) return false;
    var button = document.getElementById("button"+id.slice(cut));
    if (button == null) return false;
    button.setAttribute("class", "close-button-inner waiting");
    var a = document.getElementById("drop"+id.slice(cut));
    if (a == null) return true;
    var style = a.getAttribute("style");
    if (style.slice(-4,-3) != "5" && style.slice(-4,-3) != "o") {
        var i = 0;
        move();
    }
    return true;
}

// Function called when clicking on the "close" button in the thumbnails
function deleteBookmark (id,e)
{
    e.stopPropagation();
    var historyService = Components.classes["@mozilla.org/browser/nav-history-service;1"]
                              .getService(Components.interfaces.nsINavHistoryService);
    var db = historyService.QueryInterface(Components.interfaces.nsPIPlacesDatabase).DBConnection;
    db.createStatement("DELETE FROM moz_bookmarks WHERE id="+id+"; ").execute();
    buildPinnedPages(false);
    return true;
}

// Run the initialisation once the page has finished loading
document.addEventListener("DOMContentLoaded", buildPinnedPages, false);

// Check if the browser is connected to the internet
var xmlhttp;
function ConnectToNet(url)
{
    xmlhttp=null;
    try {
        netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserRead");
    } catch (e) { }

    if (window.XMLHttpRequest)
        xmlhttp=new XMLHttpRequest();

    if (xmlhttp!=null){
        xmlhttp.onreadystatechange=state_Change;
        xmlhttp.open("GET",url,true);
        xmlhttp.send(null);
    }
    else
        alert("Your browser does not support XMLHTTP.");
}

function state_Change()
{
    // if xmlhttp shows "loaded"
    var errorMessage = document.getElementById("alert_connection");
    var googleForm = document.getElementById("f");

    if (!navigator.onLine) {
        errorMessage.innerHTML = "Attention : Navigateur en mode hors connexion"
        googleForm.setAttribute('class', 'waiting');
        errorMessage.setAttribute('class', 'block');
        return;
    }
    else errorMessage.innerHTML = "Attention : Pas de connexion Internet";

    if (xmlhttp.readyState==4){

        try{
            // if "OK"
            if (xmlhttp.status==200){
            googleForm.setAttribute('class', 'block');
            errorMessage.setAttribute('class', 'waiting');
            return;
        }
        else{
            googleForm.setAttribute('class', 'waiting');
            errorMessage.setAttribute('class', 'block');
        }
    }
    catch(err){
        googleForm.setAttribute('class', 'waiting');
        errorMessage.setAttribute('class', 'block');
    }
    xmlhttp=null;
  }
}
