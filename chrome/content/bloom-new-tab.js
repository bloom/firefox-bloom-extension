function bloom_new_tab (event) 
{
  var new_browser = event.target.linkedBrowser;
	
  if ((new_browser.currentURI == null) && (typeof event.target.owner == "object")) 
  {
    new_browser.loadURI("bloom://accueil");
  }
}


function bloom_add_tab_open_listener (event) 
{
  var bloom_browser;
  if ((gBrowser != undefined) && (gBrowser != null))
    bloom_browser = gBrowser;
  else
    bloombrowser = getBrowser();

  gBrowser.tabContainer.addEventListener("TabOpen", bloom_new_tab, false);
}

dump ("Loading bloom-new-tab extension\n");

try 
{
  window.addEventListener ("load", bloom_add_tab_open_listener, false);
}
catch (e) 
{
    dump(e);
}
