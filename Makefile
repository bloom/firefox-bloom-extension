VERSION           = 0.1

XPI_TARGET = bloom-homepage-$(VERSION).xpi
ARCH := $(shell uname -m)
# Update the ARCH variable so that the Mozilla architectures are used
ARCH := $(shell echo ${ARCH} | sed 's/i686/x86/')

build-xpi:
	mkdir xpi
	cp install.rdf xpi/install.rdf
	sed -i 's/<em:version>.*<\/em:version>/<em:version>$(VERSION)<\/em:version>/' xpi/install.rdf
	sed -i 's/<em:targetPlatform>.*<\/em:targetPlatform>/<em:targetPlatform>Linux_$(ARCH)-gcc3<\/em:targetPlatform>/' xpi/install.rdf
	cp chrome.manifest xpi/chrome.manifest
	cp -R chrome components defaults xpi
	cd xpi && zip -r ../$(XPI_TARGET) *

build: build-xpi

clean:
	rm $(XPI_TARGET)
	rm -r xpi/
