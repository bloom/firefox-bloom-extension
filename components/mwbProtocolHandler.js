/*
 * Bloom-Web-Browser: The web browser for Bloom
 * Copyright (c) 2009, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 2.1, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 */

Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");

const Cc = Components.classes;
const Ci = Components.interfaces;

function MwbProtocolHandler()
{
}

MwbProtocolHandler.prototype = {
  classDescription : "Bloom Web Browser protocol handler",
  contractID : "@mozilla.org/network/protocol;1?name=bloom",
  classID : Components.ID("{2db2dca6-c66f-4fe3-97a9-3dcc82e86cf9}"),

  QueryInterface: XPCOMUtils.generateQI([Ci.nsIProtocolHandler]),

  scheme: "bloom",

  defaultPort: -1,

  protocolFlags: Ci.nsIProtocolHandler.URI_NOAUTH |
                 Ci.nsIProtocolHandler.URI_IS_UI_RESOURCE |
                 Ci.nsIProtocolHandler.URI_IS_LOCAL_FILE |
                 Ci.nsIProtocolHandler.URI_NON_PERSISTABLE |
                 Ci.nsIProtocolHandler.URI_IS_LOCAL_RESOURCE |
                 Ci.nsIProtocolHandler.URI_OPENING_EXECUTES_SCRIPT,


  newURI: function (aSpec, aOriginCharset, aBaseURI) {
    var new_url = Cc["@mozilla.org/network/standard-url;1"].
      createInstance (Ci.nsIStandardURL);
    new_url.init (1, -1, aSpec, aOriginCharset, aBaseURI);
    return new_url.QueryInterface(Ci.nsIURI);
  },

  newChannel: function (aURI) {
    var ios = Cc["@mozilla.org/network/io-service;1"].getService ();
    ios = ios.QueryInterface (Ci.nsIIOService);

    var uri_str = aURI.spec.substring (8);

    var suffix;
    var page;
    var matches = uri_str.match('\([^/]*\)/\(.*\)');
    if ((matches[1] != 'accueil') && (matches[1] != 'settings')) {
      page = 'accueil';
      suffix = matches[1] + matches[2];
    } else {
      page = matches[1];
      suffix = matches[2];
    }

    if (suffix.length == 0) {
      if (page == 'accueil')
        suffix = 'startpage.xhtml';
      else if (page == 'settings')
        suffix = 'settings.xhtml';
    }

    var channel = ios.newChannel('chrome://bloom/content/' + suffix, null, null);
    channel.originalURI = aURI;

    return channel;
  },

  allowPort: function (port, scheme) {
    return false;
  }
};

function NSGetModule(aCompMgr, aFileSpec)
{
  return XPCOMUtils.generateModule([MwbProtocolHandler]);
}

